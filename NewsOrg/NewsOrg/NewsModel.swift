//
//  NewsModel.swift
//  NewsOrg
//
//  Created by Abdul Mojid on 28/10/2019.
//  Copyright © 2019 Abdul Mojid. All rights reserved.
//

import Foundation
//
//struct Root: Codable {
//     let sources: [Sources]
//    let status: String
//}
//
//struct Sources: Codable {
//    let id: String
//    let name: String
//    let description: String
//    let url: String
//    let category: String
//    let language: String
//    let country: String
//    let urlsToLogos: [String: UrlsToLogos]
//    let sortBysAvailable: [String]
//
//}
//
//struct UrlsToLogos: Codable {
//    let small: String
//    let medium: String
//    let large: String
//}


struct Root: Codable {
    let sources: [Source]
    let status: String
}

struct Source: Codable {
    let category: Category
    let sortBysAvailable: [String]
}

//enum SortBysAvailable: String, Codable {
//    case top = "top"
//}

enum Category: String, Codable {
    case business = "business"
    case entertainment = "entertainment"
    case general = "general"
    case science = "science"
    case sports = "sports"
    case technology = "technology"
}



struct News {
    let category: String
    //let sortBysAvailable: [String]
}


class NewsModel {
    
    var sections: [String]
    var news = [News]()
    
    private let session: URLSession
    
    init(_ sessionConfiguration: URLSessionConfiguration = .ephemeral) {
        session = URLSession(configuration: sessionConfiguration)
        sections = []
        news = []
    }
    

    
    func saveNews(category: String /*sortBysAvailable: String*/) -> Void {
        
        let singleNews =  News.init(category: category
            /*status: status, */)
        
        news.append(singleNews)
        
    }
}


extension NewsModel {
    
    var numberOfSections: Int {
        return 1
    }
    func  numbersOfRows(inSection section: Int) -> Int {
        guard section >= 0 && section < numberOfSections else {
            return 0
        }
        
        return news.count
    }
    
    func item(atIndecPath indexPath: IndexPath) -> News? {
        guard indexPath.row >= 0 && indexPath.row < numbersOfRows(inSection: indexPath.section) else{
            return nil
        }
        return news[indexPath.row]
    }
    
    
}


