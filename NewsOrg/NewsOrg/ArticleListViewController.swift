//
//  ArticleListViewController.swift
//  NewsOrg
//
//  Created by Abdul Mojid on 28/10/2019.
//  Copyright © 2019 Abdul Mojid. All rights reserved.
//

import UIKit

class ArticleListViewController: UIViewController {
    
    
    
    @IBOutlet var articleListView: UITableView!
    
    @IBOutlet var headlineLable: UILabel!
    @IBOutlet var authorLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        
        // Do any additional setup after loading the view.
    }
    

}
