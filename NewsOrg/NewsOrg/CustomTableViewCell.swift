//
//  CustomTableViewCell.swift
//  NewsOrg
//
//  Created by Abdul Mojid on 28/10/2019.
//  Copyright © 2019 Abdul Mojid. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    
    @IBOutlet var headlineLable: UILabel!
    @IBOutlet var authorLable: UILabel!
    @IBOutlet var dateLable: UILabel!
    @IBOutlet var articleImageView: UIImageView!
    
    
    
    // Before the cell gets reused, remove the image from the imageView
    override func prepareForReuse() {
        super.prepareForReuse()
        articleImageView.image = nil
    }
    


}
