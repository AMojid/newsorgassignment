//
//  ViewController.swift
//  NewsOrg
//
//  Created by Abdul Mojid on 25/10/2019.
//  Copyright © 2019 Abdul Mojid. All rights reserved.
//Abdul

import UIKit

class ViewController: UIViewController {
    
    var model: NewsModel!
    
    @IBOutlet var tableView: UITableView!
    
  override func viewDidLoad() {
            super.viewDidLoad()
            
            model = NewsModel()

                tableView.separatorColor = UIColor.red
                   
                tableView.delegate = self
                tableView.dataSource = self
                getInformation()
            
        }
        
        
        func parseJSON(_ data: Data){
            
            do {
                
                let data = try JSONDecoder().decode(Root.self, from: data)
                
                model.news.removeAll()
                
                if data.status == "ok" {
                    self.model.sections.removeAll()
                    var allSections = [String]()
                    for (element) in data.sources.enumerated() {
                        if element.element.sortBysAvailable.contains("top"){
                            
                            allSections.append(element.element.category.rawValue.capitalized)
                            
                        }
                        
                    }
                    
                    let temp = Array(Set(allSections))
                    
                    let uniqueList = temp.sorted() { $0 < $1 }
                    
                    for (element) in uniqueList.enumerated() {
                        model.saveNews(category: element.element)
                    }
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                    
                    
                }
            } catch {
                print("JSON Error: \(error.localizedDescription)")
            }
        }
        
        func getInformation() {
             
            let urlString = "https://newsapi.org/v1/sources"
                   
                   guard let url = URL(string: urlString) else {
                       print("URL Error")
                       return
                   }
            
                   let dataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
                       
                       if let error = error {
                           print("Error - \(error.localizedDescription)")
                       } else if let data = data {
                           self.parseJSON(data)
                       } else {
                           
                           print("This should never happen")
                       }
                   }
                   dataTask.resume()
        }
                   
            
    }



    extension ViewController: UITableViewDataSource, UITableViewDelegate {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return model.news.count
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: UITableViewCell.self), for: indexPath)
            let newsItem = model.news[indexPath.row]
                
            cell.textLabel?.text = newsItem.category
            //cell.detailTextLabel?.text = album.imArtist
                return cell
            }
        
    }


